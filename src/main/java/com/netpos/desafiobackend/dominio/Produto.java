package com.netpos.desafiobackend.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
@Entity
@Table(name = "Produto")
public class Produto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	@Column(columnDefinition = "nome", nullable = false)
	private String nome;
	@Column(columnDefinition = "preco", nullable = false)
	private Double preco;
	@Column(columnDefinition = "estoque", length = 4)
	private Integer estoque;
	@Column(columnDefinition = "possui_estoque")
	private String possuiEstoque;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public Integer getEstoque() {
		return estoque;
	}
	public void setEstoque(Integer estoque) {
		this.estoque = estoque;
	}
	public String getPossuiEstoque() {
		return possuiEstoque;
	}
	public void setPossuiEstoque(String possuiEstoque) {
		this.possuiEstoque = possuiEstoque;
	}	
}
