package com.netpos.desafiobackend.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.netpos.desafiobackend.dominio.Usuario;
import com.netpos.desafiobackend.interfaces.UsuarioService;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */

@RestController
@RequestMapping(path = "/usuarios")
public class UsuarioRest {

	@Autowired
	private UsuarioService usuarioService;

	@PostMapping("/usuario")
	@ResponseStatus(value = HttpStatus.CREATED)
	public void create(@RequestBody Usuario usuario) {
		this.usuarioService.create(usuario);
	}

	@GetMapping("/usuario/{usuarioId}")
	public Usuario get(@PathVariable("usuarioId") Integer usuarioId) {
		return this.usuarioService.get(usuarioId);
	}

	@GetMapping("/")
	public List<Usuario> findAll() {
		return this.usuarioService.findAll();
	}

	@DeleteMapping("/usuario/{usuarioId}")
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable("usuarioId") Integer usuarioId) {
		this.usuarioService.delete(usuarioId);
	}

	@PutMapping("usuario")
	@ResponseStatus(value = HttpStatus.OK)
	public void update(@RequestBody Usuario usuario) {
		this.usuarioService.update(usuario);
	}
	
	@GetMapping("/usuario/filtrar")
	public Usuario findByName(@RequestParam(value="name") String name) {
		return this.usuarioService.findByName(name);
	}
}
