package com.netpos.desafiobackend.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.netpos.desafiobackend.dominio.Produto;
import com.netpos.desafiobackend.interfaces.ProdutoService;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
@RestController
@RequestMapping(path = "produtos")
public class ProdutoRest {

	@Autowired
	private ProdutoService produtoService;

	@PostMapping("produto")
	@ResponseBody
	public String create(@RequestBody Produto produto) {
	
		if(produto.getEstoque() > -1 && produto.getEstoque() <= 1000) {
			this.produtoService.create(produto);
			return "Produto criado com sucesso";
		}else {
			return "Quantidade de produto deve ser maior que zero e menor/igual a 1000";			
		}
	}

	@GetMapping("produto/{codigo}")
	public Produto get(@PathVariable("codigo") Long codigo) {
		return this.produtoService.get(codigo);
	}

	@GetMapping("/")
	public List<Produto> findAll() {
		return this.produtoService.findAll();
	}

	@DeleteMapping("produto/{codigo}")
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable("codigo") Long codigo) {
		this.produtoService.delete(codigo);
	}

	@PutMapping("produto")
	@ResponseBody
	public String update(@RequestBody Produto produto) {
		
		if(produto.getEstoque() > -1 && produto.getEstoque() <= 1000) {
			this.produtoService.update(produto);
			return "Produto criado com sucesso";
		}else {
			return "Quantidade de produto deve ser maior que zero e menor/igual a 1000";			
		}
		
		
	}
}
