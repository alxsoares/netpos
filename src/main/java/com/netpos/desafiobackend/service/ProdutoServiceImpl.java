package com.netpos.desafiobackend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netpos.desafiobackend.dominio.Produto;
import com.netpos.desafiobackend.interfaces.ProdutoRepository;
import com.netpos.desafiobackend.interfaces.ProdutoService;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
@Service
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired
	private ProdutoRepository repository;

	public void create(Produto entity) {
		this.repository.create(entity);
	}

	public List<Produto> findAll() {
		return repository.findAll();
	}

	public Produto get(Long codigo) {
		return this.repository.get(codigo);
	}

	public void delete(Long codigo) {
		this.repository.delete(codigo);
	}

	public void update(Produto entity) {
		this.repository.update(entity);
	}
}
