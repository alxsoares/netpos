package com.netpos.desafiobackend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netpos.desafiobackend.dominio.Usuario;
import com.netpos.desafiobackend.interfaces.UsuarioRepository;
import com.netpos.desafiobackend.interfaces.UsuarioService;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
@Service
public class UsuarioServiceImpl implements UsuarioService{
	
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	public UsuarioServiceImpl(UsuarioRepository usuarioRepository) {
		this.usuarioRepository = usuarioRepository;
	}
	
	public void create(Usuario entity) {
		this.usuarioRepository.create(entity);		
	}

	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}

	public Usuario get(Integer id) {		
		return this.usuarioRepository.get(id);
	}

	public void delete(Integer id) {
		this.usuarioRepository.delete(id);		
	}

	public void update(Usuario usuario) {
		this.usuarioRepository.update(usuario);		
	}
	
	public Usuario findByName(String name) {
		return this.usuarioRepository.findByName(name);
	}
}
