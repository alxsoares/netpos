package com.netpos.desafiobackend.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.springframework.stereotype.Repository;

/**
 * @author Alex
 * @version 1.0
 *
 */
@Repository
public abstract class GenericRepository<T, V> extends AbstractRepository {

	private Class<T> genericClass;
	private EntityManager em;
	private EntityTransaction etx;

	protected GenericRepository(Class<T> implclass) {
		this.genericClass = implclass;
	}

	public void persistir(T entity) {

		this.createEntityManager();
		this.createEntityTransaction();

		try {
			etx.begin();
			em.persist(entity);
			etx.commit();
		} catch (Exception e) {
			etx.rollback();
			e.printStackTrace();
		} finally {
			em.close();
		}
	}

	public T get(V pk) {

		this.createEntityManager();
		return em.find(getGenericClass(), pk);
	}

	public void update(T entity) {

		this.createEntityManager();
		this.createEntityTransaction();
		try {
			etx.begin();
			em.merge(entity);
			etx.commit();
		} catch (Exception e) {
			etx.rollback();
			e.printStackTrace();
		} finally {
			em.close();
		}

	}

	public void delete(T entity) {

		this.createEntityManager();
		this.createEntityTransaction();
		try {
			etx.begin();
			T u = em.merge(entity);
			em.remove(u);
			etx.commit();
		} catch (Exception e) {
			etx.rollback();
			e.printStackTrace();
		} finally {
			em.close();
		}
	}

	public boolean contains(T entity) {

		this.createEntityManager();

		return em.contains(entity);
	}

	public Class<T> getGenericClass() {
		return genericClass;
	}

	protected void createEntityManager() {
		em = super.getEntityManager();
	}

	protected void createEntityTransaction() {
		etx = em.getTransaction();
	}
}
