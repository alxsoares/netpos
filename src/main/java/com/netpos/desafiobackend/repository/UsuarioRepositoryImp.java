package com.netpos.desafiobackend.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.netpos.desafiobackend.dominio.Usuario;
import com.netpos.desafiobackend.interfaces.UsuarioRepository;

/**
 * @author Alex
 * @version 1.0
 *
 */
@Repository
public class UsuarioRepositoryImp extends GenericRepository<Usuario, Integer> implements UsuarioRepository {

	public UsuarioRepositoryImp() {
		super(Usuario.class);
	}

	public void create(Usuario user) {
		super.persistir(user);
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> findAll() {
		EntityManager em = this.getEntityManager();
		
		List<Usuario> usuarios = em.createQuery("FROM USUARIO").getResultList();
		return usuarios;
	}

	public Usuario get(Integer id) {
		return super.get(id);
	}

	public void delete(Integer id) {
		Usuario entity = super.get(id);
		super.delete(entity);
	}

	public void update(Usuario user) {
		Usuario u = super.get(user.getId());

		if ("".equals(user.getNome())) {
			u.setNome(user.getNome());
		}

		if (!"".equals(user.getEmail())) {
			u.setEmail(user.getEmail());
		}

		if (!"".equals(user.getSenha())) {
			u.setSenha(user.getSenha());
		}
		
		super.update(u);
	}
	
	public Usuario findByName(String name) {
		EntityManager em = super.getEntityManager();
		
		Query query = em.createQuery("FROM USUARIO p WHERE p.nome LIKE :nome");
		query.setParameter("nome", name+"%");
				
		return (Usuario) query.getSingleResult();
	}
}
