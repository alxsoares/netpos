package com.netpos.desafiobackend.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.netpos.desafiobackend.dominio.Produto;
import com.netpos.desafiobackend.interfaces.ProdutoRepository;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
@Repository
public class ProdutoRepositoryImpl extends GenericRepository<Produto, Long> implements ProdutoRepository {

	protected ProdutoRepositoryImpl() {
		super(Produto.class);
	}

	public void create(Produto entity) {
		entity.setPossuiEstoque(entity.getPossuiEstoque().toUpperCase());
		super.persistir(entity);
	}

	@SuppressWarnings("unchecked")
	public List<Produto> findAll() {
		EntityManager em = this.getEntityManager();

		List<Produto> produtos = em.createQuery("FROM Produto").getResultList();
		return produtos;
	}

	public Produto get(Long codigo) {
		return super.get(codigo);
	}

	public void delete(Long codigo) {
		Produto entity = super.get(codigo);
		super.delete(entity);
	}

	public void update(Produto entity) {
		Produto p = super.get(entity.getCodigo());

		if (!"".equals(entity.getNome())) {
			p.setNome(entity.getNome());
		}

		if (entity.getEstoque() != null) {
			p.setEstoque(entity.getEstoque());
		}

		if (!"".equals(entity.getPossuiEstoque()) && entity.getPossuiEstoque() != null) {
			p.setPossuiEstoque(entity.getPossuiEstoque());
		}

		super.update(p);
	}
}
