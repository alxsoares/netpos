package com.netpos.desafiobackend.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.springframework.stereotype.Repository;

/**
 * @author Alex
 * @version 1.0
 *
 */
@Repository
public abstract class AbstractRepository {
	
	private EntityManagerFactory emf;
	
	@PersistenceUnit
	private void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	protected EntityManager getEntityManager() {
		return emf.createEntityManager();
	}	
}
