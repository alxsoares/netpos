package com.netpos.desafiobackend.interfaces;

import java.util.List;

import com.netpos.desafiobackend.dominio.Usuario;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
public interface UsuarioService {

	void create(Usuario entity);

	List<Usuario> findAll();

	Usuario get(Integer id);

	void delete(Integer usuarioId);

	void update(Usuario usuario); 
	
	Usuario findByName(String name);

}
