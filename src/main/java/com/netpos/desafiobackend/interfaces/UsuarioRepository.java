package com.netpos.desafiobackend.interfaces;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
import java.util.List;

import com.netpos.desafiobackend.dominio.Usuario;

public interface UsuarioRepository {

	void create(Usuario entity);

	List<Usuario> findAll();

	Usuario get(Integer id);

	void delete(Integer id);

	void update(Usuario entity);
	
	Usuario findByName(String name);
}
