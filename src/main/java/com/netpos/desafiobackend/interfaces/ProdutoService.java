package com.netpos.desafiobackend.interfaces;

import java.util.List;

import com.netpos.desafiobackend.dominio.Produto;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
public interface ProdutoService {
	
	void create(Produto entity);

	List<Produto> findAll();

	Produto get(Long codigo);

	void delete(Long codigo);

	void update(Produto entity);

}
